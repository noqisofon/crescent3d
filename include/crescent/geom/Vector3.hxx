#pragma once

#include <array>
#include <iostream>
#include <string>


namespace crescent {
    namespace geom {
        /*!
         *
         */
        class Vector3 {
        public:
            double x, y, z;

        public:
           /*!
            *
            */
            Vector3();
            /*!
             *
             */
            Vector3( double x, double y, double z );
            /*!
             *
             */
            Vector3( const std::array<double, 3>& other );

        public:
           /*!
            * このベクトルの長さを計算して返します。
            */
            double magnitude() const;

            /*!
             * ベクトルの 2 乗の長さを返します。
             */
            double sqrMagnitude() const;

            /*!
             * このベクトルを正規化したものを返します。
             */
            Vector3 normalized() const;

            /*!
             *
             */
            bool equals( const Vector3 &other ) const { return this->equals( &other ); }
            /*!
             *
             */
            bool equals( const Vector3 *const &other ) const;

            /*!
             *
             */
            std::array<double, 3> toArray() const { return { this->x, this->y, this->z }; }

            /*!
             * このベクトルの値を見やすくフォーマットされた文字列を返します。
             */
            std::string toString() const;

            /*!
             * このベクトルに x、y、z の成分それぞれを設定します。
             */
            void assign( double x, double y, double z );

            /*!
             * 2 点間(from と to)の角度を返します。
             */
            static double angle(const Vector3 &from, const Vector3 &to);

            /*!
             * ベクトル同士のドット積を計算して返します。
             */
            static double dot( const Vector3 &left, const Vector3 &right );

            /*!
             *
             */
            Vector3 operator + (const Vector3& other) {
                return {
                    this->x + other.x,
                    this->y + other.y,
                    this->z + other.z
                };
            }

            /*!
             *
             */
            Vector3 operator - (const Vector3& other) {
                return {
                    this->x - other.x,
                    this->y - other.y,
                    this->z - other.z
                };
            }
        };

        /*!
         * 後ろ向きの単位ベクトルを返します。
         */
        inline Vector3 vector3_back() {
            return {  0,  0, -1 };
        }

        /*!
         *
         */
        inline Vector3 vector3_down() {
            return { 0, -1, 0 };
        }

        /*!
         *
         */
        inline Vector3 vector3_forward() {
            return { 0, 0, 1 };
        }

        /*!
         *
         */
        inline Vector3 vector3_left() {
            return { -1, 0, 0 };
        }

        /*!
         *
         */
        inline Vector3 vector3_one() {
            return { 1, 1, 1 };
        }

        /*!
         *
         */
        inline Vector3 vector3_right() {
            return { 1, 0, 0 };
        }

        /*!
         *
         */
        inline Vector3 vector3_up() {
            return { 0, 1, 0 };
        }
    }
} // namespace crescent
