#include <cmath>

#if __has_include(<format>)
#   include <format>
#else   /* __has_include(<format>) */
#   include <sstream>
#endif  /* !__has_include(<format>) */


#include "crescent/geom/Vector3.hxx"

namespace crescent {
    namespace geom {
        Vector3::Vector3() {
            this->x = 0.0;
            this->y = 0.0;
            this->z = 0.0;
        }
        Vector3::Vector3( double x, double y, double z ) {
            this->x = x;
            this->y = y;
            this->z = z;
        }

        double Vector3::magnitude() const {
            return std::sqrt( dot( *this, *this ) );
        }

        double Vector3::sqrMagnitude() const {
            return dot( *this, *this );
            // 多分、dot( v, v ) して sqrt しないのが sqrMagunitude だと思うんだが……。
        }

        Vector3 Vector3::normalized() const {
            auto this_magnitude = this->magnitude();

            return { 
                this->x / this_magnitude,
                this->y / this_magnitude,
                this->z / this_magnitude 
            };
        }

        bool Vector3::equals( const Vector3 *const &other ) const {
            return this->x == other->x &&
                this->y == other->y &&
                this->z == other->z;
        }

        std::string Vector3::toString() const {
#if __has_include(<format>)
            return std::format( "({}, {}, {})", this->x, this->y, this->z );
#else    /* __has_include(<format>) */
            std::ostringstream  output_stream;

            output_stream << "(" << this->x << ", " << this->y << ", " << this->z << ")";

            return output_stream.str();
#endif   /* !__has_include(<format>) */
        }

        void Vector3::assign( double x, double y, double z ) {
            this->x = x;
            this->y = y;
            this->z = z;
        }

        double Vector3::angle(const Vector3 &from, const Vector3 &to) {
            return std::acos( dot( from, to ) / ( from.magnitude() * to.magnitude() ) );
                
        }

        double Vector3::dot( const Vector3 &left, const Vector3 &right ) {
            return left.x * right.x +
                left.y * right.y +
                left.z * right.z;
        }
    }
}
